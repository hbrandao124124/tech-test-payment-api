using AutoMapper;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.Entities;

namespace TechTest.Payment.Borders.MappingProfiles;

public class OrderMappingProfile : Profile
{
    public OrderMappingProfile()
    {
        CreateMap<CreateOrderRequest, Order>();
        CreateMap<OrderSellerRequest, OrderSeller>();
        CreateMap<OrderItemRequest, OrderItem>();

        CreateMap<Order, OrderResponse>();
        CreateMap<Order, UpdateOrderStatusResponse>();
        CreateMap<OrderItem, OrderItemResponse>();
        CreateMap<OrderSeller, OrderSellerResponse>();
    }
}
