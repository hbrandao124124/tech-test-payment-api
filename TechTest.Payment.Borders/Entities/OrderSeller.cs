namespace TechTest.Payment.Borders.Entities;

public record OrderSeller : StoredEntity
{
    public string DocumentNumber { get; init; } = string.Empty;
    public string Name { get; init; } = string.Empty;
    public string Email { get; init; } = string.Empty;
    public string PhoneNumber { get; init; } = string.Empty;
}
