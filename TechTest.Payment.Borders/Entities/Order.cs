using System.Collections.ObjectModel;
using TechTest.Payment.Borders.Enums;

namespace TechTest.Payment.Borders.Entities;

public record Order : StoredEntity
{
    public OrderStatus Status { get; init; } = OrderStatus.PaymentPending;
    public virtual OrderSeller Seller { get; init; } = default!;
    public virtual ICollection<OrderItem> Items { get; init; } = new Collection<OrderItem>();
}
