namespace TechTest.Payment.Borders.Entities;

public record OrderItem : StoredEntity
{
    public string Name { get; init; } = string.Empty;
}
