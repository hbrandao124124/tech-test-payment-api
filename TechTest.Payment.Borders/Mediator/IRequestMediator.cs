using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.Borders.Mediator;

public interface IRequestMediator
{
    Task<TResponse> Send<TResponse>(IRequest<TResponse> request);
}
