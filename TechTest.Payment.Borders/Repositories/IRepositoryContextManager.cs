using Microsoft.EntityFrameworkCore;

namespace TechTest.Payment.Borders.Repositories;

public interface IRepositoryContextManager<TContext> where TContext : DbContext
{
    TContext GetContext();
}
