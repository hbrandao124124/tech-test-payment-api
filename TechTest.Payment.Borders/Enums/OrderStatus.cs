namespace TechTest.Payment.Borders.Enums;

public enum OrderStatus
{
    PaymentPending = 1,
    Approved,
    Dispatched,
    Delivered,
    Cancelled
}
