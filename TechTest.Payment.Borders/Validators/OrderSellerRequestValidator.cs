using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;
using TechTest.Payment.Borders.Dtos.Orders.Seller;

namespace TechTest.Payment.Borders.Validators;

public partial class OrderSellerRequestValidator : AbstractValidator<OrderSellerRequest>
{
    [GeneratedRegex("^[0-9]+$")]
    private static partial Regex OnlyDigitsRegex();

    public OrderSellerRequestValidator(IDocumentNumberValidator documentNumberValidator)
    {
        RuleFor(m => m.Email).EmailAddress(EmailValidationMode.AspNetCoreCompatible);
        RuleFor(m => m.Name).NotEmpty();
        RuleFor(m => m.PhoneNumber).Must(m => OnlyDigitsRegex().IsMatch(m)).WithMessage("Phone must only contain digits");
        RuleFor(m => m.PhoneNumber).MinimumLength(10);
        RuleFor(m => m.PhoneNumber).MaximumLength(11);
        RuleFor(m => m.DocumentNumber).NotEmpty();
        RuleFor(m => m.DocumentNumber).SetValidator(documentNumberValidator);
    }
}
