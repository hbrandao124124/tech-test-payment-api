using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders.Item;

namespace TechTest.Payment.Borders.Validators;

public class OrderItemRequestValidator : AbstractValidator<OrderItemRequest>
{
    public OrderItemRequestValidator()
    {
        RuleFor(m => m.Name).NotEmpty();
    }
}
