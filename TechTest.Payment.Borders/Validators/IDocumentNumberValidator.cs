using FluentValidation;

namespace TechTest.Payment.Borders.Validators;

public interface IDocumentNumberValidator : IValidator<string> { }
