using FluentValidation;

namespace TechTest.Payment.Borders.Validators;

public class DocumentNumberValidator : AbstractValidator<string>, IDocumentNumberValidator
{
    public DocumentNumberValidator()
    {
        RuleFor(m => m).Must(IsCpf).WithMessage("Invalid document number provided.");
    }

    private static int CalculateMod11(int sumValue)
    {
        var rest = sumValue % 11;

        if (rest < 2)
            return 0;

        return 11 - rest;
    }

    private static int CalculateVerifierSum(string input) =>
        input
            .Select((d, index) => int.Parse(d.ToString()) * (input.Length + 1 - index))
            .Sum();

    private static bool IsCpf(string input)
    {
        var digits = new string(input.Trim().Where(char.IsDigit).ToArray());

        if (digits.Length != 11)
            return false;

        for (int j = 0; j < 10; j++)
            if (j.ToString().PadLeft(11, char.Parse(j.ToString())) == digits)
                return false;

        var mainDigits = digits[..9];
        var verifierSum = CalculateVerifierSum(mainDigits);
        var firstVerifierDigit = CalculateMod11(verifierSum).ToString();

        var secondSum = CalculateVerifierSum(mainDigits + firstVerifierDigit);
        var secondVerifierDigit = CalculateMod11(secondSum).ToString();

        return input.EndsWith(firstVerifierDigit + secondVerifierDigit);
    }
}

