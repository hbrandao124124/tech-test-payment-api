using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;

namespace TechTest.Payment.Borders.Validators;

public class CreateOrderRequestValidator : AbstractValidator<CreateOrderRequest>
{
    public CreateOrderRequestValidator(IValidator<OrderSellerRequest> sellerValidator, IValidator<OrderItemRequest> itemValidator)
    {
        RuleFor(m => m.Items.Count()).GreaterThan(0).WithMessage("An order must have items");
        RuleFor(m => m.Items).ForEach(i => i.SetValidator(itemValidator));

        RuleFor(m => m.Seller).NotEmpty().WithMessage("A seller must be provided");
        RuleFor(m => m.Seller).SetValidator(sellerValidator);
    }
}
