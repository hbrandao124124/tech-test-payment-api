using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;

namespace TechTest.Payment.Borders.Validators;

public class UpdateOrderStatusRequestValidator : AbstractValidator<UpdateOrderStatusRequest>
{
    public UpdateOrderStatusRequestValidator()
    {
        RuleFor(m => m.Status).IsInEnum().WithMessage("Invalid status provided");
    }
}
