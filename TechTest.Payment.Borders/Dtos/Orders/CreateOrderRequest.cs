using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.Borders.Dtos.Orders;

public record CreateOrderRequest : IRequest<OrderResponse>
{
    public OrderSellerRequest Seller { get; init; } = default!;
    public IEnumerable<OrderItemRequest> Items { get; init; } = Enumerable.Empty<OrderItemRequest>();
}
