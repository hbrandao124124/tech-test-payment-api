namespace TechTest.Payment.Borders.Dtos.Orders.Item;

public record OrderItemRequest
{
    public string Name { get; init; } = string.Empty;
}
