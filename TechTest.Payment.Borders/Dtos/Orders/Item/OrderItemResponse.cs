namespace TechTest.Payment.Borders.Dtos.Orders.Item;

public record OrderItemResponse
{
    public Guid Id { get; init; }
    public string Name { get; init; } = string.Empty;
}
