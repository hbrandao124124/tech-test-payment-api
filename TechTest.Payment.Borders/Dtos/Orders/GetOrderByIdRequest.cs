using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.Borders.Dtos.Orders;

public record GetOrderByIdRequest : IRequest<OrderResponse>
{
    public Guid OrderId { get; init; }
}
