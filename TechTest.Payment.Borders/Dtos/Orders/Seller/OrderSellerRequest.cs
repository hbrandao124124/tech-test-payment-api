namespace TechTest.Payment.Borders.Dtos.Orders.Seller;

public record OrderSellerRequest
{
    public string DocumentNumber { get; init; } = string.Empty;
    public string Name { get; init; } = string.Empty;
    public string Email { get; init; } = string.Empty;
    public string PhoneNumber { get; init; } = string.Empty;
}
