namespace TechTest.Payment.Borders.Dtos.Orders.Seller;

public record OrderSellerResponse
{
    public Guid Id { get; init; }
    public string DocumentNumber { get; init; } = string.Empty;
    public string Name { get; init; } = string.Empty;
    public string Email { get; init; } = string.Empty;
    public string PhoneNumber { get; init; } = string.Empty;
}
