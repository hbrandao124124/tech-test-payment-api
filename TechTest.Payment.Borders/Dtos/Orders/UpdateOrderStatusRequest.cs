using System.Text.Json.Serialization;
using TechTest.Payment.Borders.Enums;
using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.Borders.Dtos.Orders;

public record UpdateOrderStatusRequest : IRequest<UpdateOrderStatusResponse>
{
    // Ignoring so that the controller parameter is preferred
    [JsonIgnore]
    public Guid OrderId { get; init; }
    public OrderStatus Status { get; init; }
}
