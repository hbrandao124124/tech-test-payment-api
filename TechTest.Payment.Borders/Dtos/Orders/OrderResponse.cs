using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.Enums;

namespace TechTest.Payment.Borders.Dtos.Orders;

public record OrderResponse
{
    public Guid Id { get; init; }
    public DateTime CreatedAt { get; init; }
    public OrderStatus Status { get; init; }
    public OrderSellerResponse Seller { get; init; } = default!;
    public IEnumerable<OrderItemResponse> Items { get; init; } = Enumerable.Empty<OrderItemResponse>();
}
