using TechTest.Payment.Borders.Enums;

namespace TechTest.Payment.Borders.Dtos.Orders;

public record UpdateOrderStatusResponse
{
    public OrderStatus Status { get; init; }
}
