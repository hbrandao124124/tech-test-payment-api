using Microsoft.EntityFrameworkCore;
using TechTest.Payment.Borders.Repositories;

namespace TechTest.Payment.Repositories;

public class RepositoryContextManager<TContext> : IRepositoryContextManager<TContext> where TContext : DbContext
{
    private readonly IDbContextFactory<TContext> _contextFactory;

    public RepositoryContextManager(IDbContextFactory<TContext> contextFactory)
    {
        _contextFactory = contextFactory;
    }

    public TContext GetContext() => _contextFactory.CreateDbContext();
}
