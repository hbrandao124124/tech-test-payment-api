using Microsoft.EntityFrameworkCore;
using TechTest.Payment.Borders.Entities;

namespace TechTest.Payment.Repositories.Contexts;

public class PaymentContext : DbContext
{
    public DbSet<Order> Orders { get; set; } = default!;

    public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Order>().HasKey(m => m.Id);
        modelBuilder.Entity<Order>().HasMany(m => m.Items);
        modelBuilder.Entity<Order>().HasOne(m => m.Seller);

        base.OnModelCreating(modelBuilder);
    }
}
