using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Shared.Exceptions;

namespace TechTest.Payment.Repositories;

public class Repository<TEntity, TContext> : IRepository<TEntity> where TEntity : StoredEntity where TContext : DbContext
{
    private readonly IRepositoryContextManager<TContext> _contextManager;

    public Repository(IRepositoryContextManager<TContext> contextManager)
    {
        _contextManager = contextManager;
    }

    public async Task Add(TEntity entity)
    {
        try
        {
            using var context = _contextManager.GetContext();

            context.Set<TEntity>().Add(entity);

            await context.SaveChangesAsync();
        }
        catch (DbUpdateException ex) when (ex.InnerException is SqliteException dbError)
        {
            if (dbError.SqlState == "23505")
                throw new UniqueViolationException($"There already is a {typeof(TEntity).Name} with the provided information.");
            throw dbError;
        }
    }

    public async Task<TEntity?> GetById(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>>? queryExtensions = null)
    {
        using var context = _contextManager.GetContext();
        var query = queryExtensions?.Invoke(context.Set<TEntity>()) ?? context.Set<TEntity>();
        return await query.FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<TEntity?> Update(TEntity entity)
    {
        try
        {
            using var context = _contextManager.GetContext();
            var existing = await context.Set<TEntity>().FirstOrDefaultAsync(e => e.Id == entity.Id);

            if (existing is null) return null;

            context.Entry(existing).CurrentValues.SetValues(entity);

            await context.SaveChangesAsync();

            return entity;
        }
        catch (DbUpdateException ex) when (ex.InnerException is SqliteException dbError)
        {
            if (dbError?.SqlState == "23505")
                throw new UniqueViolationException($"There already is a {typeof(TEntity).Name} with the provided information.");
            throw dbError ?? throw ex;
        }
    }
}
