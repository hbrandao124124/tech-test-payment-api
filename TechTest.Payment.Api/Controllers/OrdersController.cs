using Microsoft.AspNetCore.Mvc;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Mediator;
using TechTest.Payment.Shared.Models;

namespace TechTest.Payment.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OrdersController : ControllerBase
{
    private readonly IRequestMediator _mediator;

    public OrdersController(IRequestMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(OrderResponse))]
    [ProducesErrorResponseType(typeof(ErrorMessage[]))]
    public async Task<IResult> CreateOrder([FromBody] CreateOrderRequest request)
        => Results.CreatedAtRoute(value: await _mediator.Send(request));

    [HttpGet("{OrderId}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(OrderResponse))]
    [ProducesErrorResponseType(typeof(ErrorMessage[]))]
    public async Task<IResult> GetOrderById([FromRoute] GetOrderByIdRequest request)
        => Results.Ok(await _mediator.Send(request));

    [HttpPatch("{OrderId}/status")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UpdateOrderStatusResponse))]
    [ProducesErrorResponseType(typeof(ErrorMessage[]))]
    public async Task<IResult> UpdateOrderStatus([FromRoute] Guid OrderId, [FromBody] UpdateOrderStatusRequest request)
        => Results.Ok(await _mediator.Send(request with { OrderId = OrderId }));
}
