using Microsoft.EntityFrameworkCore;
using Serilog;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Repositories;
using TechTest.Payment.Repositories.Contexts;

namespace TechTest.Payment.Api.Configurations;

public static class DatabaseConfiguration
{
    public static IServiceCollection AddDatabase(this IServiceCollection services, string connectionString, string migrationsAssembly)
    {
        if (!Directory.Exists($"{AppContext.BaseDirectory}/Database"))
            Directory.CreateDirectory($"{AppContext.BaseDirectory}/Database");

        services.AddDbContextFactory<PaymentContext>(options => options
            .UseSqlite(
                string.Format(connectionString ?? throw new ArgumentNullException(nameof(connectionString)), AppContext.BaseDirectory),
                options => options.MigrationsAssembly(migrationsAssembly))
            .EnableDetailedErrors()
            .LogTo(action: Log.Warning, minimumLevel: LogLevel.Warning));

        services.AddSingleton<IRepositoryContextManager<PaymentContext>, RepositoryContextManager<PaymentContext>>();

        return services;
    }
}
