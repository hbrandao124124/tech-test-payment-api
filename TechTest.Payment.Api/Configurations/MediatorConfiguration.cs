using TechTest.Payment.Borders.Mediator;

namespace TechTest.Payment.Api.Configurations;

public static class MediatorConfiguration
{
    public static IServiceCollection AddMediator(this IServiceCollection services)
        => services.AddSingleton<IRequestMediator, RequestMediator>();
}
