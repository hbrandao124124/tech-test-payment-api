using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Repositories;
using TechTest.Payment.Repositories.Contexts;

namespace TechTest.Payment.Api.Configurations;

public static class RepositoryConfiguration
{
    public static IServiceCollection AddRepositories(this IServiceCollection services)
        =>
            services.AddSingleton<IRepository<Order>, Repository<Order, PaymentContext>>()
        ;
}
