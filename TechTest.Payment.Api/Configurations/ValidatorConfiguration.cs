using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Api.Configurations;

public static class ValidatorConfiguration
{
    public static IServiceCollection AddValidators(this IServiceCollection services)
        =>
            services
                .AddSingleton<IValidator<CreateOrderRequest>, CreateOrderRequestValidator>()
                .AddSingleton<IValidator<UpdateOrderStatusRequest>, UpdateOrderStatusRequestValidator>()
                .AddSingleton<IValidator<OrderItemRequest>, OrderItemRequestValidator>()
                .AddSingleton<IValidator<OrderSellerRequest>, OrderSellerRequestValidator>()
                .AddSingleton<IDocumentNumberValidator, DocumentNumberValidator>()
        ;
}
