using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.UseCases;
using TechTest.Payment.UseCases.Orders;

namespace TechTest.Payment.Api.Configurations;

public static class UseCaseConfiguration
{
    public static IServiceCollection AddUseCases(this IServiceCollection services)
        =>
            services.AddTransient<IRequestHandler<CreateOrderRequest, OrderResponse>, CreateOrderUseCase>()
                    .AddTransient<IRequestHandler<GetOrderByIdRequest, OrderResponse>, GetOrderByIdUseCase>()
                    .AddTransient<IRequestHandler<UpdateOrderStatusRequest, UpdateOrderStatusResponse>, UpdateOrderStatusUseCase>()
        ;
}
