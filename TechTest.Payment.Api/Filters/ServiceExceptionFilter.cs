using System.Net;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog.Context;
using TechTest.Payment.Shared.Constants;
using TechTest.Payment.Shared.Exceptions;
using TechTest.Payment.Shared.Models;

namespace TechTest.Payment.Api.Filters;

public class ServiceExceptionFilter : IExceptionFilter
{
    private readonly ILogger<ServiceExceptionFilter> _logger;

    public ServiceExceptionFilter(ILogger<ServiceExceptionFilter> logger)
    {
        _logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        var (code, errorMessage, errors) = context.Exception switch
        {
            KeyNotFoundException ex =>
            (
                HttpStatusCode.NotFound,
                ex.Message,
                new ErrorMessage[] { new(ErrorCodes.NotFound, ex.Message) }
            ),
            UnauthorizedAccessException ex =>
            (
                HttpStatusCode.Unauthorized,
                ex.Message,
                new ErrorMessage[] { new(ErrorCodes.ValidationError, ex.Message) }
            ),
            ValidationException ex =>
            (
                HttpStatusCode.BadRequest,
                ex.Message,
                ex.Errors
                    .Select(error => new ErrorMessage(ErrorCodes.ValidationError, error.ErrorMessage))
                    .DefaultIfEmpty(new ErrorMessage(ErrorCodes.ValidationError, ex.Message))
                    .ToArray()
            ),
            UniqueViolationException ex =>
            (
                HttpStatusCode.Conflict,
                ex.Message,
                new ErrorMessage[] { new(ErrorCodes.Conflict, ex.Message) }
            ),
            _ =>
            (
                HttpStatusCode.InternalServerError,
                context.Exception.Message,
                new ErrorMessage[] { new(ErrorCodes.UnhandledError, "Internal server error") }
            )
        };

        if (errorMessage is not null)
            using (LogContext.PushProperty("Errors", errors))
                _logger.LogError("An error occurred during the request: {@ErrorMessage}", errorMessage);

        context.HttpContext.Response.StatusCode = (int)code;
        context.HttpContext.Response.ContentType = "application/json";

        context.Result = new JsonResult(errors);
    }
}
