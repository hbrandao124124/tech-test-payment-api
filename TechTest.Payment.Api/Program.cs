using System.Globalization;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Serilog;
using TechTest.Payment.Api.Configurations;
using TechTest.Payment.Api.Filters;
using TechTest.Payment.Borders.MappingProfiles;
using TechTest.Payment.Repositories.Contexts;

var builder = WebApplication.CreateBuilder(args);

var loggerConfig = new LoggerConfiguration()
    .ReadFrom
    .Configuration(builder.Configuration);

CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");

Log.Logger = loggerConfig.CreateLogger();
builder.Host.UseSerilog();

Log.Information("Starting TechTest.Payment.Api");

builder.Services.AddRouting(options => options.LowercaseUrls = true);
builder.Services
    .AddControllers(options => options.Filters.Add<ServiceExceptionFilter>())
    .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
builder.Services.ConfigureHttpJsonOptions(options =>
{
    options.SerializerOptions.Converters.Add(new JsonStringEnumConverter());
});
builder.Services.AddSwaggerGen();

builder.Services.AddDatabase(
    builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new ApplicationException("No connection string configured"),
    "TechTest.Payment.Api"
);
builder.Services.AddAutoMapper(options =>
{
    options.AddProfile<OrderMappingProfile>();
});

builder.Services.AddRepositories();
builder.Services.AddValidators();
builder.Services.AddUseCases();
builder.Services.AddMediator();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger(options =>
    {
        options.RouteTemplate = "api-docs/{documentName}/open-api.json";
    });

    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/api-docs/v1/open-api.json", "GLM.AgvSupervisor.Core v1");
        options.RoutePrefix = "api-docs";
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using var serviceScope = app.Services.CreateScope();
serviceScope.ServiceProvider.GetService<PaymentContext>()?.Database.Migrate();

app.Run();
