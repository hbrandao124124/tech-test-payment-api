using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.Borders.Mediator;

public class RequestMediator : IRequestMediator
{
    private readonly ILogger<RequestMediator> _logger;
    private readonly IServiceProvider _serviceProvider;

    public RequestMediator(ILogger<RequestMediator> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
    }

    public async Task<TResponse> Send<TResponse>(IRequest<TResponse> request)
    {
        var handlerType = typeof(IRequestHandler<,>).MakeGenericType(request.GetType(), typeof(TResponse));
        var requestHandler = (dynamic)_serviceProvider.GetRequiredService(handlerType);

        _logger.LogInformation("Matched handler {HandlerType} for request {RequestType}", handlerType, request.GetType());

        return await requestHandler.Handle((dynamic)request);
    }
}
