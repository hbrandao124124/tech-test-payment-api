namespace TechTest.Payment.Shared.Constants;

public static partial class ErrorCodes
{
    public const string UnhandledError = "00";
    public const string NotFound = "01";
    public const string Conflict = "02";
    public const string BadGateway = "03";
    public const string ValidationError = "04";
}
