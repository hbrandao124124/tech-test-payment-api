namespace TechTest.Payment.Shared.Exceptions;

[Serializable]
public class UniqueViolationException : Exception
{
    public UniqueViolationException(string message) : base(message) { }
}
