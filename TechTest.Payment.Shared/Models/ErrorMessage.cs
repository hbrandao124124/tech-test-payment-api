namespace TechTest.Payment.Shared.Models;

public record ErrorMessage(string Code, string Message);
