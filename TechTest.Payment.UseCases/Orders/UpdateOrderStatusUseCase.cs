using AutoMapper;
using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Borders.UseCases;
using TechTest.Payment.UseCases.Constants;

namespace TechTest.Payment.UseCases.Orders;

public class UpdateOrderStatusUseCase : IRequestHandler<UpdateOrderStatusRequest, UpdateOrderStatusResponse>
{
    private readonly IMapper _mapper;
    private readonly IRepository<Order> _orderRepository;
    private readonly IValidator<UpdateOrderStatusRequest> _validator;

    public UpdateOrderStatusUseCase(IMapper mapper, IRepository<Order> orderRepository, IValidator<UpdateOrderStatusRequest> validator)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _validator = validator;

    }

    public async Task<UpdateOrderStatusResponse> Handle(UpdateOrderStatusRequest request)
    {
        _validator.ValidateAndThrow(request);

        var order = await _orderRepository.GetById(request.OrderId)
            ?? throw new KeyNotFoundException("Order not found");

        var allowedStatuses = OrderOperations.AllowedStatusChangesMap.TryGetValue(order.Status, out var allowedStatusesResult)
            ? allowedStatusesResult
            : throw new ValidationException($"An order with status {order.Status} cannot be updated");

        if (!allowedStatuses.Contains(request.Status))
            throw new ValidationException($"An order with status {order.Status} cannot be changed to {request.Status}. Allowed: {string.Join(",", allowedStatuses)}");

        var updatedOrder = await _orderRepository.Update(order with { Status = request.Status });

        return _mapper.Map<UpdateOrderStatusResponse>(updatedOrder!);
    }
}
