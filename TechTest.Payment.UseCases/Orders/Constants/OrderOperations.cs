using TechTest.Payment.Borders.Enums;

namespace TechTest.Payment.UseCases.Constants;

public static class OrderOperations
{
    public static readonly Dictionary<OrderStatus, IEnumerable<OrderStatus>> AllowedStatusChangesMap = new()
    {
        { OrderStatus.PaymentPending, new[] { OrderStatus.Approved, OrderStatus.Cancelled } },
        { OrderStatus.Approved, new[] { OrderStatus.Dispatched, OrderStatus.Cancelled } },
        { OrderStatus.Dispatched, new[] { OrderStatus.Delivered } },
    };
}
