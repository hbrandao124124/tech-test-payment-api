using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.UseCases.Orders;

public class GetOrderByIdUseCase : IRequestHandler<GetOrderByIdRequest, OrderResponse>
{
    private readonly IMapper _mapper;
    private readonly IRepository<Order> _orderRepository;

    public GetOrderByIdUseCase(IMapper mapper, IRepository<Order> orderRepository)
    {
        _mapper = mapper;
        _orderRepository = orderRepository;
    }

    public async Task<OrderResponse> Handle(GetOrderByIdRequest request)
    {
        var order = await _orderRepository.GetById(request.OrderId, orders => orders.Include(o => o.Seller).Include(o => o.Items))
            ?? throw new KeyNotFoundException($"Order {request.OrderId} not found");

        return _mapper.Map<OrderResponse>(order);
    }
}
