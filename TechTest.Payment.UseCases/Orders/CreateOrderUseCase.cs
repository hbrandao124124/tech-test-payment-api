using AutoMapper;
using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Borders.UseCases;

namespace TechTest.Payment.UseCases.Orders;

public class CreateOrderUseCase : IRequestHandler<CreateOrderRequest, OrderResponse>
{
    private readonly IValidator<CreateOrderRequest> _validator;
    private readonly IMapper _mapper;
    private readonly IRepository<Order> _orderRepository;

    public CreateOrderUseCase(IValidator<CreateOrderRequest> validator, IMapper mapper, IRepository<Order> orderRepository)
    {
        _validator = validator;
        _mapper = mapper;
        _orderRepository = orderRepository;
    }

    public async Task<OrderResponse> Handle(CreateOrderRequest request)
    {
        _validator.ValidateAndThrow(request);

        var order = _mapper.Map<Order>(request);

        await _orderRepository.Add(order);

        return _mapper.Map<OrderResponse>(order);
    }
}
