using FluentValidation.TestHelper;
using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Tests.Validators;

public class OrderItemRequestValidatorTests
{
    private readonly OrderItemRequestValidator _validator;

    public OrderItemRequestValidatorTests()
    {
        _validator = new OrderItemRequestValidator();
    }

    [Fact]
    public void Validate_WithName_IsValid()
    {
        var request = new OrderItemRequest()
        {
            Name = "Name"
        };
        var result = _validator.TestValidate(request);
        result.ShouldNotHaveAnyValidationErrors();
    }

    [Fact]
    public void Validate_WithEmptyName_IsInvalid()
    {
        var request = new OrderItemRequest();
        var result = _validator.TestValidate(request);
        result.ShouldHaveValidationErrorFor(m => m.Name);
    }
}
