using FluentValidation;
using FluentValidation.TestHelper;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Tests.Validators;

public class OrderSellerRequestValidatorTests
{
    private class MockDocumentValidator : InlineValidator<string>, IDocumentNumberValidator { }

    private readonly OrderSellerRequestValidator _validator;

    public OrderSellerRequestValidatorTests()
    {
        _validator = new OrderSellerRequestValidator(new MockDocumentValidator());
    }

    [Fact]
    public void Validate_WithValidParameters_IsValid()
    {
        var request = new OrderSellerRequest()
        {
            Email = "test@test.com",
            Name = "Name",
            PhoneNumber = "12123451234",
            DocumentNumber = "Number"
        };
        var result = _validator.TestValidate(request);
        result.ShouldNotHaveAnyValidationErrors();
    }

    [Fact]
    public void Validate_WithEmptyParameters_IsInvalid()
    {
        var request = new OrderSellerRequest();

        var result = _validator.TestValidate(request);

        result.ShouldHaveValidationErrorFor(m => m.Email);
        result.ShouldHaveValidationErrorFor(m => m.Name);
        result.ShouldHaveValidationErrorFor(m => m.PhoneNumber);
        result.ShouldHaveValidationErrorFor(m => m.DocumentNumber);
    }
}
