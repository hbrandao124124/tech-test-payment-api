using FluentValidation.TestHelper;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Enums;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Tests.Validators;

public class UpdateOrderStatusRequestValidatorTests
{
    private readonly UpdateOrderStatusRequestValidator _validator;

    public UpdateOrderStatusRequestValidatorTests()
    {
        _validator = new UpdateOrderStatusRequestValidator();
    }

    [Theory]
    [InlineData(OrderStatus.PaymentPending)]
    [InlineData(OrderStatus.Cancelled)]
    [InlineData(OrderStatus.Approved)]
    [InlineData(OrderStatus.Delivered)]
    [InlineData(OrderStatus.Dispatched)]
    public void Validate_WithExistingStatuses_IsValid(OrderStatus status)
    {
        var request = new UpdateOrderStatusRequest { OrderId = Guid.NewGuid(), Status = status };
        var result = _validator.TestValidate(request);

        result.ShouldNotHaveAnyValidationErrors();
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(0)]
    [InlineData(6)]
    [InlineData(7)]
    [InlineData(8)]
    public void Validate_WithOutOfBoundsStatuses_IsInvalid(int status)
    {
        var request = new UpdateOrderStatusRequest { OrderId = Guid.NewGuid(), Status = (OrderStatus)status };

        var result = _validator.TestValidate(request);

        result.ShouldHaveValidationErrorFor(m => m.Status);
    }
}
