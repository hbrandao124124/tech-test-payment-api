using FluentValidation.TestHelper;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Tests.Validators;

public class DocumentNumberValidatorTests
{
    private readonly DocumentNumberValidator _validator;

    public DocumentNumberValidatorTests()
    {
        _validator = new DocumentNumberValidator();
    }

    [Theory]
    [InlineData("676.782.330-94")]
    [InlineData("426.756.800-60")]
    [InlineData("847.601.270-54")]
    [InlineData("028.778.430-41")]
    [InlineData("542.609.680-50")]
    [InlineData("54260968050")]
    [InlineData("504.928fasfa70063")]
    public void Validate_OnValidInputs_IsValid(string cpf)
    {
        var result = _validator.TestValidate(cpf);
        result.ShouldNotHaveAnyValidationErrors();
    }

    [Theory]
    [InlineData("")]
    [InlineData("50492.00-6")]
    [InlineData("504.928.700-6")]
    [InlineData("9138656138")]
    [InlineData("01234567891")]
    [InlineData("00000000000")]
    [InlineData("11111111111")]
    [InlineData("22222222222")]
    [InlineData("33333333333")]
    [InlineData("44444444444")]
    [InlineData("55555555555")]
    [InlineData("66666666666")]
    [InlineData("77777777777")]
    [InlineData("88888888888")]
    [InlineData("99999999999")]
    [InlineData("12345678910")]
    [InlineData("13168513")]
    [InlineData("1864513ugfaiasfj")]
    [InlineData("1864513ugfaiasfjfhausd")]
    [InlineData("fisahfasiashg301hasf")]
    public void Validate_OnInvalidInputs_IsInvalid(string cpf)
    {
        var result = _validator.TestValidate(cpf);
        result.ShouldHaveValidationErrorFor(m => m);
    }
}
