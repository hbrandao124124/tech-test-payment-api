using FluentValidation;
using FluentValidation.TestHelper;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Dtos.Orders.Item;
using TechTest.Payment.Borders.Dtos.Orders.Seller;
using TechTest.Payment.Borders.Validators;

namespace TechTest.Payment.Tests.Validators;

public class CreateOrderRequestValidatorTests
{
    private readonly CreateOrderRequestValidator _validator;

    public CreateOrderRequestValidatorTests()
    {
        _validator = new CreateOrderRequestValidator(new InlineValidator<OrderSellerRequest>(), new InlineValidator<OrderItemRequest>());
    }

    [Fact]
    public void Validate_WithMissingData_HasErrorForMissingProperties()
    {
        var request = new CreateOrderRequest
        {
            Items = Enumerable.Empty<OrderItemRequest>(),
            Seller = null!
        };

        var result = _validator.TestValidate(request);

        result.ShouldHaveValidationErrorFor(m => m.Items.Count());
        result.ShouldHaveValidationErrorFor(m => m.Seller);
    }

    [Fact]
    public void Validate_WithValidRequest_HasNoErrors()
    {
        var request = new CreateOrderRequest
        {
            Items = Enumerable.Range(0, 20).Select(_ => new OrderItemRequest()),
            Seller = new()
        };

        var result = _validator.TestValidate(request);

        result.ShouldNotHaveAnyValidationErrors();
    }
}
