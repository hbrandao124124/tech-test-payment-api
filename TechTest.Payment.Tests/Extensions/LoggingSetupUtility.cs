using Microsoft.Extensions.Logging;

namespace TechTest.Payment.Tests.Extensions;

public static class LoggingSetupExtensions
{
    public static void SetupLoggingCalls<TCategory>(this Mock<ILogger<TCategory>> logger, int numberOfCalls)
    {
        for (int i = 0; i < numberOfCalls; i++)
            logger
                .Setup(l =>
                    l.Log(
                        It.IsAny<LogLevel>(),
                        It.IsAny<EventId>(),
                        It.IsAny<It.IsAnyType>(),
                        It.IsAny<Exception?>(),
                        It.IsAny<Func<It.IsAnyType, Exception?, string>>())
                )
                .Verifiable();
    }
}
