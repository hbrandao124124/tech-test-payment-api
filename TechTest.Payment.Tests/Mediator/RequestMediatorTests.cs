using Microsoft.Extensions.Logging;
using TechTest.Payment.Borders.Mediator;
using TechTest.Payment.Borders.UseCases;
using TechTest.Payment.Tests.Extensions;

namespace TechTest.Payment.Tests.Mediator;

public class RequestMediatorTests
{
    private readonly MockRepository _mockRepository = new(MockBehavior.Strict);
    private readonly Mock<ILogger<RequestMediator>> _logger;
    private readonly Mock<IServiceProvider> _serviceProvider;
    private readonly RequestMediator _mediator;

    public RequestMediatorTests()
    {
        _logger = _mockRepository.Create<ILogger<RequestMediator>>();
        _serviceProvider = _mockRepository.Create<IServiceProvider>();

        _mediator = new(_logger.Object, _serviceProvider.Object);
    }

    public record RequestMock : IRequest<bool>;
    public class RequestHandlerMock : IRequestHandler<RequestMock, bool>
    {
        public Task<bool> Handle(RequestMock request) => Task.FromResult(false);
    }

    [Fact]
    public async Task Send_WhenRequestHasHandler_ReturnsHandleResult()
    {
        _logger.SetupLoggingCalls(1);
        _serviceProvider.Setup(p => p.GetService(It.IsAny<Type>())).Returns(new RequestHandlerMock());

        var result = await _mediator.Send(new RequestMock());

        result.Should().Be(false);

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }

    [Fact]
    public async Task Send_WhenRequestDoesNotHaveHandler_ThrowsInvalidOperationException()
    {
        _serviceProvider.Setup(p => p.GetService(It.IsAny<Type>())).Returns(null as object);

        var action = async () => await _mediator.Send(new RequestMock());

        await action.Should().ThrowAsync<InvalidOperationException>();

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }
}
