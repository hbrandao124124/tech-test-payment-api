using AutoMapper;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.UseCases.Orders;

namespace TechTest.Payment.Tests.UseCases;

public class GetOrderByIdUseCaseTests
{
    private static readonly MockRepository _mockRepository = new(MockBehavior.Strict);
    private static readonly Mock<IMapper> _mapper = _mockRepository.Create<IMapper>();
    private static readonly Mock<IRepository<Order>> _orderRepository = _mockRepository.Create<IRepository<Order>>();
    private static readonly GetOrderByIdUseCase _useCase = new(_mapper.Object, _orderRepository.Object);

    [Fact]
    public async Task Handle_WhenOrderIsNotFound_ThrowsKeyNotFoundException()
    {
        var request = new GetOrderByIdRequest();

        _orderRepository.Setup(r => r.GetById(request.OrderId, It.IsAny<Func<IQueryable<Order>, IQueryable<Order>>>())).Returns(Task.FromResult(null as Order));

        var action = async () => await _useCase.Handle(request);

        await action.Should().ThrowAsync<KeyNotFoundException>($"Order {request.OrderId} not found");

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }

    [Fact]
    public async Task Handle_WhenOrderIsFound_ReturnsOrderResponse()
    {
        var request = new GetOrderByIdRequest();
        var order = new Order();
        var orderResponse = new OrderResponse();

        _orderRepository.Setup(r => r.GetById(request.OrderId, It.IsAny<Func<IQueryable<Order>, IQueryable<Order>>>())).Returns(Task.FromResult((Order?)order));
        _mapper.Setup(m => m.Map<OrderResponse>(order)).Returns(orderResponse);

        var result = await _useCase.Handle(request);

        result.Should().Be(orderResponse);

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }
}
