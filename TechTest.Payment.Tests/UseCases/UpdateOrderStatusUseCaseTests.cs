using AutoMapper;
using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Enums;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.Tests.UseCases.Data;
using TechTest.Payment.UseCases.Orders;

namespace TechTest.Payment.Tests.UseCases;

public class UpdateOrderStatusUseCaseTests
{
    private readonly MockRepository _mockRepository = new(MockBehavior.Strict);
    private readonly Mock<IMapper> _mapper;
    private readonly Mock<IRepository<Order>> _orderRepository;
    private readonly UpdateOrderStatusUseCase _useCase;

    public UpdateOrderStatusUseCaseTests()
    {
        _mapper = _mockRepository.Create<IMapper>();
        _orderRepository = _mockRepository.Create<IRepository<Order>>();
        _useCase = new(_mapper.Object, _orderRepository.Object, new InlineValidator<UpdateOrderStatusRequest>());
    }

    [Fact]
    public async Task Handle_WhenOrderIsNotFound_ThrowsKeyNotFoundException()
    {
        var request = new UpdateOrderStatusRequest();

        _orderRepository.Setup(r => r.GetById(request.OrderId, null)).Returns(Task.FromResult(null as Order));

        var action = async () => await _useCase.Handle(request);

        await action.Should().ThrowAsync<KeyNotFoundException>();

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }

    [Theory]
    [MemberData(nameof(OrderStatusDataGenerator.GetInvalidKeys), MemberType = typeof(OrderStatusDataGenerator))]
    public async Task Handle_WhenStatusIsNotChangeable_ThrowsValidationException(OrderStatus status)
    {
        var order = new Order { Status = status };
        var request = new UpdateOrderStatusRequest();

        _orderRepository.Setup(r => r.GetById(request.OrderId, null)).Returns(Task.FromResult((Order?)order));

        var action = async () => await _useCase.Handle(request);

        await action.Should().ThrowAsync<ValidationException>();

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }

    [Theory]
    [MemberData(nameof(OrderStatusDataGenerator.GetInvalidValuesForValidKeys), MemberType = typeof(OrderStatusDataGenerator))]
    public async Task Handle_WhenNewStatusIsNotAllowed_ThrowsValidationException(OrderStatus currentValidStatus, OrderStatus unallowedStatus)
    {
        var order = new Order { Status = currentValidStatus };
        var request = new UpdateOrderStatusRequest { Status = unallowedStatus };

        _orderRepository.Setup(r => r.GetById(request.OrderId, null)).Returns(Task.FromResult((Order?)order));

        var action = async () => await _useCase.Handle(request);

        await action.Should().ThrowAsync<ValidationException>();

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }

    [Theory]
    [MemberData(nameof(OrderStatusDataGenerator.GetValidStatusChanges), MemberType = typeof(OrderStatusDataGenerator))]
    public async Task Handle_WhenStatusChangeIsValid_ReturnsNewStatus(OrderStatus currentValidStatus, OrderStatus newValidStatus)
    {
        var order = new Order { Status = currentValidStatus };
        var request = new UpdateOrderStatusRequest { Status = newValidStatus };
        var updatedOrder = new Order { Status = newValidStatus };
        var response = new UpdateOrderStatusResponse { Status = newValidStatus };

        _orderRepository.Setup(r => r.GetById(request.OrderId, null)).Returns(Task.FromResult((Order?)order));
        _orderRepository.Setup(r => r.Update(It.IsAny<Order>())).Returns(Task.FromResult((Order?)updatedOrder));
        _mapper.Setup(m => m.Map<UpdateOrderStatusResponse>(updatedOrder)).Returns(response);

        var result = await _useCase.Handle(request);

        result.Should().Be(response);

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }
}
