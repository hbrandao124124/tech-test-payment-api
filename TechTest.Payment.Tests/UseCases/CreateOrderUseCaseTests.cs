using AutoMapper;
using FluentValidation;
using TechTest.Payment.Borders.Dtos.Orders;
using TechTest.Payment.Borders.Entities;
using TechTest.Payment.Borders.Repositories;
using TechTest.Payment.UseCases.Orders;

namespace TechTest.Payment.Tests.UseCases;

public class CreateOrderUseCaseTests
{
    private static readonly MockRepository _mockRepository = new(MockBehavior.Strict);
    private static readonly InlineValidator<CreateOrderRequest> _validator = new();
    private static readonly Mock<IMapper> _mapper = _mockRepository.Create<IMapper>();
    private static readonly Mock<IRepository<Order>> _orderRepository = _mockRepository.Create<IRepository<Order>>();
    private static readonly CreateOrderUseCase _useCase = new(_validator, _mapper.Object, _orderRepository.Object);

    [Fact]
    public async Task Handle_WhenValidationFails_ThrowsValidationException()
    {
        _validator.RuleFor(m => m).Must(m => false);

        var result = async () => await _useCase.Handle(new CreateOrderRequest());

        await result.Should().ThrowAsync<ValidationException>();
    }

    [Fact]
    public async Task Handle_WhenSuccessfullyAdded_ReturnsOrderResponse()
    {
        var order = new Order();
        var request = new CreateOrderRequest();
        var orderResponse = new OrderResponse();

        _mapper.Setup(m => m.Map<Order>(request)).Returns(order);
        _orderRepository.Setup(r => r.Add(order)).Returns(Task.FromResult(order));
        _mapper.Setup(m => m.Map<OrderResponse>(order)).Returns(orderResponse);

        var result = await _useCase.Handle(request);

        result.Equals(orderResponse);

        _mockRepository.VerifyAll();
        _mockRepository.VerifyNoOtherCalls();
    }
}
