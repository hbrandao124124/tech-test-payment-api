using System.Collections;
using TechTest.Payment.Borders.Enums;
using TechTest.Payment.UseCases.Constants;

namespace TechTest.Payment.Tests.UseCases.Data;

public class OrderStatusDataGenerator : IEnumerable<object[]>
{
    public static IEnumerable<object[]> GetInvalidKeys() =>
        Enum.GetValues<OrderStatus>()
            .Where(v => !OrderOperations.AllowedStatusChangesMap.ContainsKey(v))
            .Select(v => new object[] { v });

    public static IEnumerable<object[]> GetInvalidValuesForValidKeys() =>
        OrderOperations.AllowedStatusChangesMap.Keys
            .SelectMany(key =>
                Enum.GetValues<OrderStatus>()
                    .Where(v => !OrderOperations.AllowedStatusChangesMap[key].Contains(v))
                    .Select(invalidNewStatus => new object[] { key, invalidNewStatus })
            );

    public static IEnumerable<object[]> GetValidStatusChanges() =>
        OrderOperations.AllowedStatusChangesMap.Keys
            .SelectMany(key =>
                OrderOperations.AllowedStatusChangesMap[key]
                    .Select(allowedStatus => new object[] { key, allowedStatus }));

    public IEnumerator<object[]> GetEnumerator() => GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
